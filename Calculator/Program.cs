﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main()
        {
            while (true)
            {
                Console.Clear();

                double a, b, result;
                string c;

                try
                {
                    Console.WriteLine("Enter first number");
                    a = double.Parse(Console.ReadLine());

                    Console.WriteLine("Enter second number");
                    b = double.Parse(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid number!");
                    Console.ReadLine();
                    continue;
                }

                Console.WriteLine("Enter operation '+' '-' '*' '/'");
                c = Console.ReadLine();

                switch (c)
                {
                    case "+":
                        result = a + b;
                        Console.WriteLine($"{a} + {b} = {result}");
                        break;
                    case "-":
                        result = a - b;
                        Console.WriteLine($"{a} - {b} = {result}");
                        break;
                    case "*":
                        result = a * b;
                        Console.WriteLine($"{a} * {b} = {result}");
                        break;
                    case "/":
                        if (b == 0)
                        {
                            Console.WriteLine("You can't divide by zero!");
                        }
                        else
                        {
                            result = a / b;
                            Console.WriteLine($"{a} / {b} = {result}");
                        }
                        break;
                    default:
                        Console.WriteLine("Invalid operation!");
                        break;
                }

                Console.ReadLine();
            }
        }
    }
}
